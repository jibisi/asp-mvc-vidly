namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    public partial class PopulateNameOfMembershipTypes : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE MembershipTypes SET Name = 'Pay As You Go' WHERE DurationInMonths = 0");
            Sql("UPDATE MembershipTypes SET Name = 'Monthly' WHERE DurationInMonths > 0 AND DurationInMonths < 4");
            Sql("UPDATE MembershipTypes SET Name = 'Quarterly' WHERE DurationInMonths > 3 AND DurationInMonths < 12");
            Sql("UPDATE MembershipTypes SET Name = 'Annual' WHERE DurationInMonths = 12");
        }
        public override void Down()
        {
            Sql("UPDATE MembershipTypes SET Name = '' ");
        }
    }
}
