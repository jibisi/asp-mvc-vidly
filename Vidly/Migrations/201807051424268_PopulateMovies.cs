namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateMovies : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Movies (Name, GenreId, DateAdded, ReleaseDate, NumberInStock) VALUES ('Hangover', 5, CAST('4/5/2016' AS DATETIME), CAST('6/5/2009' AS DATETIME), 5)");
            Sql("INSERT INTO Movies (Name, GenreId, DateAdded, ReleaseDate, NumberInStock) VALUES ('Die Hard', 1, CAST('6/8/2016' AS DATETIME), CAST('7/20/1988' AS DATETIME), 2)");
            Sql("INSERT INTO Movies (Name, GenreId, DateAdded, ReleaseDate, NumberInStock) VALUES ('The Terminator', 1, CAST('2/2/2017' AS DATETIME), CAST('10/26/1984' AS DATETIME), 4)");
            Sql("INSERT INTO Movies (Name, GenreId, DateAdded, ReleaseDate, NumberInStock) VALUES ('Toy Story', 3, CAST('4/6/2017' AS DATETIME), CAST('11/22/1995' AS DATETIME), 7)");
            Sql("INSERT INTO Movies (Name, GenreId, DateAdded, ReleaseDate, NumberInStock) VALUES ('Titanic', 4, CAST('8/12/2016' AS DATETIME), CAST('12/19/1997' AS DATETIME), 5)");
        }
        
        public override void Down()
        {
            Sql("DELETE FROM Movies");
        }
    }
}
