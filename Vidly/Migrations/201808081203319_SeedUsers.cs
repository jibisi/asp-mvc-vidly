namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'132eda67-488a-4cbc-adcd-551e01f41828', N'guest@vidly.la', 0, N'AA5WbUPaQqxSXe2+rxjI/4nY1AMsCRMOh07r/fO3zR+o8AuPpvsLmz2vCvXoX9nnJQ==', N'ec428251-ff0f-4c38-9e6c-32aa661bc234', NULL, 0, 0, NULL, 1, 0, N'guest@vidly.la')
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'8272bf5f-9176-4698-8c56-c0e5df4da246', N'admin@vidly.com', 0, N'ANw0bYde59I2U7uLiXDY27IyfBwmIfpIUAbKl9NSRy36/MCdT0YuVI0rxxY9WZYMoA==', N'80a957c8-e3a7-4479-aaee-dbb6d2ebc8b2', NULL, 0, 0, NULL, 1, 0, N'admin@vidly.com')
                
                INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'a4aae62c-76f0-43c9-8cac-c712678643f4', N'CanManageMovies')

                INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'8272bf5f-9176-4698-8c56-c0e5df4da246', N'a4aae62c-76f0-43c9-8cac-c712678643f4')

            ");
        }
        
        public override void Down()
        {
        }
    }
}
