﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Vidly.Models;
using Vidly.Dtos;

namespace Vidly.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Customer, CustomerDto>().ReverseMap().ForMember(c => c.Id, opt => opt.Ignore());
            //CreateMap<CustomerDto, Customer>();

            CreateMap<Movie, MovieDto>().ReverseMap().ForMember(m => m.Id, opt => opt.Ignore());

            CreateMap<MembershipType, MembershipTypeDto>().ReverseMap();

            CreateMap<Genre, GenreDto>().ReverseMap();
        }
    }
}